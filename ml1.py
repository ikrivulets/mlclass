import math
import matplotlib.pyplot as plt
from random import random, shuffle


def generate_dots(centerX1, centerX2, size, maxr, name):    
    points = []
    for i in range(size):
        r = random()*maxr
        alf = random()*(2*math.pi)
        x1 = centerX1 + r*math.sin(alf)
        x2 = centerX2 + r*math.cos(alf)
        points.append((x1, x2, name))
        
    return points
    

def count_weights(points, exitNeuronNum):
    shuffle(points)
    alf = 0.01
    w1 = w2 = T = 0.1
    cur_epoch = 0
    good_epoch = False
    while(not good_epoch):
        good_epoch = True
        for cur_point in points:
            S = cur_point[0]*w1 + cur_point[1]*w2 - T
            y = 0 if S <=0 else 1
            name_e = (cur_point[2] >> exitNeuronNum) % 2
            K = y - name_e
            w1 = w1 - (alf * cur_point[0] * K)
            w2 = w2 - (alf * cur_point[1] * K)
            T = T + (alf * K)
            if K != 0:
                good_epoch = False
        cur_epoch = cur_epoch + 1
        if (good_epoch):
            break
#         if (cur_epoch > 1000):
#             break
    a = -w1/w2
    b = T/w2
    return a, b


a = generate_dots(3, 9, 220, 2.4, 0)
b = generate_dots(3, 4, 500, 2.4, 1)
c = generate_dots(3, -1, 530, 2.4, 3)

d = []
d.extend(a)
d.extend(b)
d.extend(c)

print(a)

l1, m1 = count_weights(d, 0)
l2, m2 = count_weights(d, 1)


a = zip(*a)
b = zip(*b)
c = zip(*c)


plt.plot(a[0], a[1], 'ro')
plt.plot(b[0], b[1], 'bo')
plt.plot(c[0], c[1], 'yo')
plt.plot((-10, 10), (-10*l1+m1, 10*l1+m1),color='black')
plt.plot((-10, 10), (-10*l2+m2, 10*l2+m2),color='black')

plt.axis([-10, 10, -10, 10])

plt.show()




